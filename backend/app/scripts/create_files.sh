DIR="./app/scripts/new_files"
if [ -d "$DIR" ]; then
  # Take action if $DIR exists. #
  echo "Copying files in ${DIR}..."
  for i in `seq 1 100`; 
  do 
    cp ./app/scripts/test.txt ./app/scripts/new_files/"test$i.txt";
    echo "File test$i.txt created."  
    sleep 1
  done
else
  ###  Control will jump here if $DIR does NOT exists ###
  echo "Error: ${DIR} not found. Can not continue."
  exit 1
fi