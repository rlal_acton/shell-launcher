const express = require('express')
const app = express();
var cors = require('cors');

const http = require('http');

// const startSonar = require.resolve('C:\\Program Files\\sonarqube-8.3.1.34397\\bin\\windows-x86-64\\StartSonar.bat');
const startCreateFiles = require.resolve('./scripts/create_files.sh');
const { spawn, exec } = require('child_process');
const socketIO = require('socket.io');
const server = http.createServer(app);
const io = socketIO(server);

io.origins('*:*');

io.on('connection', (socket) => {
    console.log('user connected');
    socket.on('disconnect', () => {
        console.log('user disconnected');
    });
    socket.on('new-message', (message) => {
        io.emit('new-message', message);
    });
});

app.get('/create-files', cors(), (req, res) => {
    const sh = exec(startCreateFiles);
    sh.stdout.on('data', (data) => {
        console.log(data.toString());
        io.emit('new-message', data.toString());
    });

    sh.stderr.on('data', (data) => {
        console.error(data.toString());
        io.emit('new-message', data.toString());
    });

    sh.on('exit', (code) => {
        console.log(`Child exited with code ${code}`);
        res.end(JSON.stringify(`{"responseMessage": "Child exited with code ${code}", "code": ${code}}`));
    });
});

// app.get('/sonar', cors(), (req, res) => {
//     const bat = spawn(startSonar);
//     bat.stdout.on('data', (data) => {
//         console.log(data.toString());
//         io.emit('new-message', data.toString());
//     });

//     bat.stderr.on('data', (data) => {
//         console.error(data.toString());
//     });

//     bat.on('exit', (code) => {
//         console.log(`Child exited with code ${code}`);
//         res.end(JSON.stringify(`{"responseMessage": "Child exited with code ${code}", "code": ${code}}`));
//     });
// });

server.listen(3000);