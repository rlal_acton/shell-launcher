import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { ScriptLauncherService } from 'src/app/services/script-launcher/script-launcher.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-main-screen',
  templateUrl: './main-screen.component.html',
  styleUrls: ['./main-screen.component.scss']
})
export class MainScreenComponent implements OnInit, OnChanges {

  @Input() script;
  @Output() executingScript = new EventEmitter();
  loading = false;
  scriptResult = '';
  scriptError = '';
  messageList: string[] = [];

  constructor(private scriptLauncherService: ScriptLauncherService) { }

  ngOnInit(): void {
    this.initMessages();
    this.scriptLauncherService
      .getScriptMessages()
      .subscribe((message: string) => {
        this.messageList.push(message);
      });
  }

  ngOnChanges() {
    this.initMessages();
  }

  initMessages() {
    this.scriptResult = '';
    this.scriptError = '';
    this.messageList = [];
  }

  executeScript() {
    this.initMessages();
    this.loading = true;
    this.executingScript.emit({id: this.script.id, state: 'ongoing'});
    this.scriptLauncherService.launchScript(this.script.id).pipe(take(1)).subscribe(
      result => {
        const parsedResult = JSON.parse(result);
        this.loading = false;
        if (parsedResult.code === 0) {
          this.scriptResult = parsedResult.responseMessage;
          this.executingScript.emit({id: this.script.id, state: 'success'});          
        } else {
          this.scriptError = parsedResult.responseMessage;
          this.executingScript.emit({id: this.script.id, state: 'error'});
        }
      },
      error => {
        this.scriptError = error;
        this.loading = false;
        this.executingScript.emit({id: this.script.id, state: 'error'});
      }
    );
  }

}
