import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit {

  @Output() scriptSelected = new EventEmitter();
  @Input() scriptsList;

  constructor() { }

  ngOnInit(): void {
  }

  selectScript(id: string) {
    this.scriptSelected.emit(id);
  }

}
