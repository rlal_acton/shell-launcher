import { Component, ViewChild } from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'frontend-ui';
  scriptSelected;
  @ViewChild('drawer') public sidenav: MatDrawer;

  constructor() {}

  scriptsList = [
    {
      id: 'create-files',
      title: 'Create Files',
      description: 'This script will create 100 copies of the app/scripts/test.txt file and place them into app/scripts/new_files folder',
      state: 'idle'
    }
  ];

  onMenuClick() {
    this.sidenav.toggle();
  }

  onScriptSelected(id: string) {
    this.scriptsList.forEach((script) => {
      if (script.id === id) {
        this.scriptSelected = script;
      }
    });    
  }

  onExecutingScript(event: any) {
    this.scriptsList.forEach((script, index, object) => {
      if (script.id === event.id) {
        object[index].state = event.state;
      }
    });
  }
}
