import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Socket } from 'ngx-socket-io';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ScriptLauncherService {

  host = environment.backendHost;

  constructor(private httpClient: HttpClient, private socket: Socket) { }

  launchScript(id: string): Observable<any> {
    return this.httpClient
      .get(`${this.host}/${id}`)
      .pipe(map(response => {
        return response;
      }),
        catchError(this.handleError('launchScript')));
  }

  sendMessage(message) {
    this.socket.emit('new-message', message);
  }

  getScriptMessages = () => {
    return Observable.create((observer) => {
      this.socket.on('new-message', (message) => {
        observer.next(message);
      });
    });
  }

  private handleError(operation: string) {
    return (err: any) => {
      const errMsg = `error in ${operation}() retrieving ${this.host}`;
      console.log(`${errMsg}:`, err);
      if (err instanceof HttpErrorResponse) {
        console.log(`status: ${err.status}, ${err.statusText}`);
      }
      throw (errMsg);
    };
  }
}
